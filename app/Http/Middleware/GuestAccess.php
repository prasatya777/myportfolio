<?php

namespace App\Http\Middleware;

use App\Http\Helpers\Api\GeolocationApi;
use App\Http\Helpers\GeolocationHelpers;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GuestAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $channelLogFileGuestMyPotofolio = Log::build([
            'driver' => 'single',
            'path'   => storage_path('logs/guestMyPortofolio/Guest_'.date("d-m-Y").'.log'),
            'level'  => 'info'
        ]);

        $resGeolocationApi = GeolocationApi::getGeolocationIp($request->ip());
        if($resGeolocationApi->status == true){
            $reformatGeoLocationApi = GeolocationHelpers::convertSingleObject($resGeolocationApi->data);
            if($reformatGeoLocationApi->status == true){
                Log::stack(['slack',$channelLogFileGuestMyPotofolio])->info( 'Orignial Ip : '.$request->ip(), array($reformatGeoLocationApi->data));
                return $next($request);
            }
        }
        Log::stack(['slack',$channelLogFileGuestMyPotofolio])->info( 'Orignial Ip : '.$request->ip(), array('message'=> 'Failed Get Detail GeoLocation IP'));
        return $next($request);
    }
}
