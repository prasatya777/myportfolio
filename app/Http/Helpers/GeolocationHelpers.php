<?php


namespace App\Http\Helpers;

use DOMDocument;
use Illuminate\Support\Facades\Log;

class GeolocationHelpers{
    public static function syntaxHighlight($json) {
        try {
            $json = str_replace(['&', '<', '>'], ['&amp;', '&lt;', '&gt;'], $json);
            $replaceData = preg_replace_callback(
                '/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/',
                function($match) {
                    $cls = 'number';
                    if (preg_match('/^"/', $match[0])) {
                        if (preg_match('/:/', $match[0])) {
                            $cls = 'key';
                        } else {
                            $cls = 'string';
                        }
                    } else if (preg_match('/true|false/', $match[0])) {
                        $cls = 'boolean';
                    } else if (preg_match('/null/', $match[0])) {
                        $cls = 'null';
                    }
                    return '<span class="' . $cls . '">' . $match[0] . '</span>';
                },
                $json
            );
            if($replaceData){
                return BuildResponseHelper::buildApiFuncResponse(true, 'Success syntaxHighlight', $replaceData);
            }
            return BuildResponseHelper::buildApiFuncResponse(false, 'Failed syntaxHighlight');
        } catch (\Exception $e) {
            $channelLogFileError = Log::build([
                'driver' => 'single',
                'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                'level'  => 'error'
            ]);
            $channelLogErrorSlackMyPotofolio = Log::build([
                'driver'   => 'slack',
                'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                'username' => 'prasatya777',
                'emoji'    => ':boom:',
                'level'    => 'error',
            ]);
            Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                'prev'    => $e->getPrevious(),
                'code'    => $e->getCode(),
                'trace'   => $e->getTrace(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
            ));
            return BuildResponseHelper::buildApiFuncResponse(false, 'Error on Try Catch Func : syntaxHighlight');
        }
    }

    public static function orderResponse($data) {
        try {
            $out = [];
            $order = [
                "query",
                "message",
                "status",
                "continent",
                "continentCode",
                "country",
                "countryCode",
                "region",
                "regionName",
                "city",
                "district",
                "zip",
                "lat",
                "lon",
                "timezone",
                "offset",
                "currency",
                "isp",
                "org",
                "as",
                "asname",
                "reverse",
                "mobile",
                "proxy",
                "hosting"
            ];
            foreach ($order as $key) {
                if (isset($data[$key])) {
                    $out[$key] = $data[$key];
                }
            }
            if(is_array($out)){
                if(count($out) > 0){
                    return BuildResponseHelper::buildApiFuncResponse(true, 'Success orderResponse', $out);
                }
            }
            return BuildResponseHelper::buildApiFuncResponse(false, 'Failed orderResponse');
        } catch (\Exception $e) {
            $channelLogFileError = Log::build([
                'driver' => 'single',
                'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                'level'  => 'error'
            ]);
            $channelLogErrorSlackMyPotofolio = Log::build([
                'driver'   => 'slack',
                'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                'username' => 'prasatya777',
                'emoji'    => ':boom:',
                'level'    => 'error',
            ]);
            Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                'prev'    => $e->getPrevious(),
                'code'    => $e->getCode(),
                'trace'   => $e->getTrace(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
            ));
            return BuildResponseHelper::buildApiFuncResponse(false, 'Error on Try Catch Func : orderResponse');
        }
    }

    public static function convertSingleObject($dataHtml){
        try {
            $response      = unserialize($dataHtml);
            $orderResponse = GeolocationHelpers::orderResponse($response);
            if($orderResponse->status == false){
                return BuildResponseHelper::buildApiFuncResponse(false, $orderResponse->message);
            }
            $json          = json_encode($orderResponse->data, JSON_PRETTY_PRINT);


            $reformat = GeolocationHelpers::syntaxHighlight($json);
            if($reformat->status == false){
                return BuildResponseHelper::buildApiFuncResponse(false, $orderResponse->message);
            }

            // Create a DOMDocument and load the HTML string
            $dom = new DOMDocument();
            $dom->loadHTML($reformat->data, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

            // Initialize an empty array to store key-value pairs
            $data = [];

            // Find all <span> elements with class "key"
            $keyNodes = $dom->getElementsByTagName('span');
            foreach ($keyNodes as $keyNode) {
                if ($keyNode->getAttribute('class') === 'key') {
                    $key       = trim($keyNode->textContent, '"');
                    $valueNode = $keyNode->nextSibling;

                    while ($valueNode && $valueNode->nodeName !== 'span') {
                        $valueNode = $valueNode->nextSibling;
                    }

                    if ($valueNode && $valueNode->getAttribute('class') === 'string') {
                        $value = trim($valueNode->textContent, '"');
                        $data[$key] = $value;
                    }
                }
            }

            // Convert the data array to an object
            $dataObject = (object) $data;
            if($dataObject){
                return BuildResponseHelper::buildApiFuncResponse(true, 'Success Reformat Ip GeoLocation', $dataObject);
            }
            return BuildResponseHelper::buildApiFuncResponse(false, 'Failed Reformat Ip GeoLocation');
        } catch (\Exception $e) {
            $channelLogFileError = Log::build([
                'driver' => 'single',
                'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                'level'  => 'error'
            ]);
            $channelLogErrorSlackMyPotofolio = Log::build([
                'driver'   => 'slack',
                'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                'username' => 'prasatya777',
                'emoji'    => ':boom:',
                'level'    => 'error',
            ]);
            Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                'prev'    => $e->getPrevious(),
                'code'    => $e->getCode(),
                'trace'   => $e->getTrace(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
            ));
            return BuildResponseHelper::buildApiFuncResponse(false, 'Error on Try Catch Func : convertSingleObject');
        }
    }
}
