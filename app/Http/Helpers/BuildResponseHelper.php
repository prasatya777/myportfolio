<?php
namespace App\Http\Helpers;

// use App\Models\API\Partner\UserSession;
// use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Redis;
// use Illuminate\Support\Facades\URL;

class BuildResponseHelper{
    public static function buildApiSuccResponse($code=400, $status=false, $message='Failed Response', $data=array(), $errors=null){
        $response          = new \stdClass();
        $response->status  = $status;
        $response->message = $message;
        $response->errors  = $errors;
        $response->data    = $data;

        return response()->json($response, $code);
    }

    public static function buildApiErrResponse($code=400, $status=false, $message='Failed Response',  $errors=null, $data=null){
        $response          = new \stdClass();
        $response->status  = $status;
        $response->message = $message;
        $response->errors  = $errors;
        $response->data    = $data;

        return response()->json($response, $code);
    }

    public static function buildApiFuncResponse($status=false, $message='Failed Response',  $data=null){
        $response          = new \stdClass();
        $response->status  = $status;
        $response->message = $message;
        $response->data    = $data;

        return $response;
    }

    // public static function checkAccessHelper($url_value, $collectUserSession){
    //     // REVIEW Check Authorized Url
    //         $baseURL        = URL::to('/');
    //         $urlDestination = str_replace($baseURL, '', $url_value);

    //         $userId = $collectUserSession->data->user_id;
    //         $route   = DB::table('user_route')
    //             ->where('u_id', $userId)
    //             ->get()
    //             ->toArray();


    //     foreach ($route as $key => $value) {
    //         if(preg_match("/\{(.*)\}/", $value->route, $matches)) {
    //             $check = str_replace($matches, '', $value->route);
    //             $routes[] = str_replace('/', '', $check);
    //         } else {
    //             $routes[] = str_replace('/', '', $value->route);
    //         }
    //     }

    //     $urlDestination = str_replace('/', '', $urlDestination);

    //     if($collectUserSession->status && in_array($urlDestination, $routes)) {
    //         return BuildResponseHelper::buildApiFuncResponse(true,'Authorized',[]);
    //     }
    //     return BuildResponseHelper::buildApiFuncResponse(false,'Unauthorized');
    // }
}
