<?php


namespace App\Http\Helpers\Api;

use App\Http\Helpers\BuildResponseHelper;
use Illuminate\Support\Facades\Log;

class GeolocationApi{

    private function mainCurl($endpoint, $params = [], $method='POST') {
        try {
            $host  = env('GEOLOCATION_API_URL');
            $token = env('GEOLOCATION_API_TOKEN');

            $url     = null;
            $payload = null;
            $headers = null;

            $url = $host.$endpoint;

            if($method == 'POST'){
                $params['token'] = $token;
                $payload         = json_encode($params);
                $headers         = array(
                    'Content-type: application/json',
                );
            }else if($method == 'GET'){
                $headers         = array(
                    'Content-type: application/json',
                    // 'token: '.$token
                );
            }


            // create curl resource
            $ch = curl_init();

            // set url
            curl_setopt($ch, CURLOPT_URL, $url);
            // dd($url, $params, GeolocationApi::headers,$method);
            //return the transfer as a string
            curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $method );

            if($payload != null) {
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
            }
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_COOKIEJAR, "" );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt( $ch, CURLOPT_ENCODING, "" );
            curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false ); // required for https urls
            // curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 5 );
            // curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
            curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );

            if(is_array($headers) && !empty($headers)) {
                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
            }

            // $output contains the output string
            $output = curl_exec($ch);

            // dd($output, $params, $headers);
            // close curl resource to free up system resources
            curl_close($ch);

            if($output){
                return BuildResponseHelper::buildApiFuncResponse(true, 'Api Success', $output);
            }

            return BuildResponseHelper::buildApiFuncResponse(false, 'Api Failed');

        } catch (\Exception $e) {
            $channelLogFileError = Log::build([
                'driver' => 'single',
                'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                'level'  => 'error'
            ]);
            $channelLogErrorSlackMyPotofolio = Log::build([
                'driver'   => 'slack',
                'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                'username' => 'prasatya777',
                'emoji'    => ':boom:',
                'level'    => 'error',
            ]);
            Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                'prev'    => $e->getPrevious(),
                'code'    => $e->getCode(),
                'trace'   => $e->getTrace(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
            ));
            return BuildResponseHelper::buildApiFuncResponse(false, 'Error on Try Catch Func : getGeolocationIp');
        }
	}

    public static function getGeolocationIp($ipAddress){
        try {
            $params = array(
                'fields' => 'status,message,continent,continentCode,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,offset,currency,isp,org,as,asname,reverse,mobile,proxy,hosting,query'
            );

            $url = '/'.$ipAddress;
            if(is_array($params)){
                if(count($params) > 0){
                    $queryUrl = http_build_query($params);
                    $result   = GeolocationApi::mainCurl($url.'?'.$queryUrl, $params, 'GET', true);
                    if($result->status == false){
                        return BuildResponseHelper::buildApiFuncResponse(false, 'Failed Get Data');
                    }
                    return BuildResponseHelper::buildApiFuncResponse(true, 'Success Get Data', $result->data);
                }
            }

            $result = GeolocationApi::mainCurl($url, [], 'GET');
            if($result->status == false){
                return BuildResponseHelper::buildApiFuncResponse(false, 'Failed Get Data');
            }
            return BuildResponseHelper::buildApiFuncResponse(true, 'Success Get Data', $result->data);
        } catch (\Exception $e) {
            $channelLogFileError = Log::build([
                'driver' => 'single',
                'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                'level'  => 'error'
            ]);
            $channelLogErrorSlackMyPotofolio = Log::build([
                'driver'   => 'slack',
                'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                'username' => 'prasatya777',
                'emoji'    => ':boom:',
                'level'    => 'error',
            ]);
            Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                'prev'    => $e->getPrevious(),
                'code'    => $e->getCode(),
                'trace'   => $e->getTrace(),
                'line'    => $e->getLine(),
                'file'    => $e->getFile(),
                'message' => $e->getMessage(),
            ));
            return BuildResponseHelper::buildApiFuncResponse(false, 'Error on Try Catch Func : getGeolocationIp');
        }
    }

    // public function masterPhoneBookUpdate($params=array()){
    //     $url    = 'masterPhoneBook/dashboard/update';
    //     $result = GeolocationApi::mainCurl($url, $params, 'POST');
    //     $result = json_decode($result);
    //     return $result;
    // }

}
