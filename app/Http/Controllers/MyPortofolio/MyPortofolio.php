<?php

namespace App\Http\Controllers\MyPortofolio;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Api\GeolocationApi;
use App\Http\Helpers\GeolocationHelpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MyPortofolio extends Controller
{

    // public function __construct(Request $request)
    // {
    //     parent::__construct();
    //     $this->request = $request;
    // }

    // NOTE This For Navigation
        public function index_view(){
            return $this->get_list_view();
        }

    // NOTE This For Blade
        // REVIEW This For List Blade
            protected function get_list_view(){
                try{
                    $jsonString           = file_get_contents(resource_path('json/myportofolio/myportofolio.json'));
                    $data['json_project'] = json_decode($jsonString);

                    return view('myportofolio.list', $data);
                } catch (\Exception $e) {
                    $channelLogFileError = Log::build([
                        'driver' => 'single',
                        'path'   => storage_path('logs/errors/Error|'.date("d-m-Y").'.log'),
                        'level'  => 'error'
                    ]);
                    $channelLogErrorSlackMyPotofolio = Log::build([
                        'driver'   => 'slack',
                        'url'      => env('LOG_SLACK_WEBHOOK_ERRORS_URL'),
                        'username' => 'prasatya777',
                        'emoji'    => ':boom:',
                        'level'    => 'error',
                    ]);
                    Log::stack([$channelLogErrorSlackMyPotofolio, $channelLogFileError])->error($e->getMessage(), array(
                        'prev'    => $e->getPrevious(),
                        'code'    => $e->getCode(),
                        'trace'   => $e->getTrace(),
                        'line'    => $e->getLine(),
                        'file'    => $e->getFile(),
                        'message' => $e->getMessage(),
                    ));
                }
            }

}
