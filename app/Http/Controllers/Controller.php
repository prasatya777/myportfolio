<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $app_name;
    protected $app_url;
    protected $title;

    public function __construct()
    {
        $this->app_name = config('constant.APP_NAME');
        $this->app_url = config('constant.APP_URL');

        $this->initialize_view();
    }

    private function initialize_view()
    {
        View::share('app_name', $this->app_name);
        View::share('app_url', $this->app_url);
        View::share('title', $this->title);
    }
}
