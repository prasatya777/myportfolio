@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="col-sm-12 pl-sm-4 pt-3 pb-3">
        <p class="h4"><i class="fa fa-list"></i> @lang('myportofolio/myportofolio.heading_title')</p>
    </div>

    <div class="panel-body col-sm-12 offset-sm-auto pt-0">
        @foreach ($json_project as $key_json_project=>$item_json_project)
            <div class="card mb-sm-4">
                <div class="card-header h4 font-weight-bold">
                    {{$item_json_project->title_name}}
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="carousel{{$key_json_project}}" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @foreach ($item_json_project->image as $item_image_li)
                                        <li data-target="#carousel{{$key_json_project}}" data-slide-to={{$item_image_li->slide_to}}  class="{{$item_image_li->slide_to == 0 ? 'active' : '' }}"></li>
                                    @endforeach
                                </ol>

                                <div class="carousel-inner">
                                    @foreach ($item_json_project->image as $item_image)
                                        <div class="{{$item_image->slide_to == 0 ? 'carousel-item active' : 'carousel-item'}}">
                                            <img class="d-block w-100" src="{{$app_url}}{{$item_image->img_src}}" alt="{{$item_image->alt_name}}">
                                            <div class="carousel-caption d-none d-md-block font-weight-bold {{$item_image->text_color}} ">
                                                <h4><span class="badge badge-secondary">{{$item_image->tittle_img}}</span></h4>
                                                <p><span class="badge badge-secondary">{{$item_image->desc_img}}</span></p>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                                <a class="carousel-control-prev" href="#carousel{{$key_json_project}}" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel{{$key_json_project}}" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>

                        </div>


                        <div class="col-sm-6">
                            <h5 class="card-title">{{$item_json_project->title_name}}</h5>
                            <p class="card-text">{{$item_json_project->detail_text}}</p>

                            @if(!empty($item_json_project->link_demo))
                                <p class="card-text font-weight-bold">{{$item_json_project->acces_login}}</p>
                                <a href="{{empty($item_json_project->main_link) ? $app_url.$item_json_project->link_demo : $item_json_project->link_demo}}" target="_blank" class="btn btn-primary mb-sm-3">{{$item_json_project->button_demo}}</a>
                            @endif

                            @if(!empty($item_json_project->li_detail_job))
                                <ul class="list-group list-group-flush">
                                    @foreach ($item_json_project->li_detail_job as $item_detail_job)
                                        <li class="list-group-item">{{$item_detail_job}}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <div>
                                @foreach ($item_json_project->li_detail_tool as $item_detail_tool)
                                    <div class="badge badge-secondary mt-sm-2 p-sm-2">{{$item_detail_tool}}</div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>

                <div class="card-footer text-muted">
                </div>
            </div>
        @endforeach
    </div>
</div>


@endsection

@section('script')
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $('.carousel').carousel();
    </script>
@endsection
