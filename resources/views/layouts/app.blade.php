<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow">

        <title>{{$app_name}}</title>

        <link rel="stylesheet" href="{{$app_url}}/css/app.css">
        {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
        /* Make the image fully responsive */
        .carousel-inner img {
          width: 100%;
          height: 100%;
        }
        </style>


        <style>
            .header-background-color{
                background-color:#EAECEE;
            }

            .fa-icon {
                padding: 12px;
                font-size: 22px;
                text-align: center;
                text-decoration: none;
                margin: 5px 2px;
                border-radius: 50%;
            }

            .fa:hover {
                opacity: 0.7;
            }

            .fa-linkedin {
                background: #007bb5;
                color: white;
            }

            .fa-envelope {
                background: #cb2027;
                color: white;
            }

            .fa-gitlab {
                background: #dd4b39;
                color: white;
            }

            .fa-whatsapp{
                background:#2ECC71;
                color: white;
            }



        </style>

        @yield('css')
    </head>
    <body>
        <div id="container" class="container-fluid p-0">

            @include('layouts.header')

            <div id="content">
                @yield('content')
            </div>
            <hr>
            @include('layouts.footer')
        </div>

        @yield('script')

    </body>
</html>
