<div class="container-fluid header-background-color">
    <div class="row px-sm-5 mx-sm-5">

        <div class="col-sm-8 pt-sm-5 mb-0">
            <p class="h1">Assalamualaikum,</p>
            <p class="h4">Hi, I'm Andika Prasatya</p>
            <p>Software Engineer from Indonesia who wants to learn new things, explore every new tech and also Software Engineer is my passion. I have experience in building a website. This is My Portfolio</p>
            <div>
                <a href="https://www.linkedin.com/in/andika-prasatya/" target="_blank" class="fa fa-linkedin fa-icon" data-toggle="tooltip" title="@lang('myportofolio/myportofolio.tex_linkedin')"></a>
                <a href="https://gitlab.com/prasatya777" target="_blank" class="fa fa-gitlab fa-icon" data-toggle="tooltip" title="@lang('myportofolio/myportofolio.text_gitlab')"></a>
                <a href="https://wa.me/6281904644860" target="_blank" class="fa fa-whatsapp fa-icon" data-toggle="tooltip" title="@lang('myportofolio/myportofolio.text_whatsapp')"></a>
                <a href="mailto:prasatya777@gmail.com" target="_blank" class="fa fa-envelope fa-icon" data-toggle="tooltip" title="@lang('myportofolio/myportofolio.text_envelope')"></a>
            </div>
        </div>

        <div class="col-sm-4 pt-sm-2 px-sm-5 d-flex justify-content-center">
            <img src= "{{$app_url}}/image/my-grayscale-crop.png" style="max-height: 20em;" alt="...">
        </div>

    </div>
</div>


