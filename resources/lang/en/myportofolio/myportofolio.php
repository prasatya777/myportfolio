<?php

return [
    'text_envelope' => 'prasatya777@gmail.com',
    'text_whatsapp' => '+6281904644860',
    'tex_linkedin'  => 'https://www.linkedin.com/in/andika-prasatya/',
    'heading_title' => 'List My Work',
    'text_footer'   => '&copy; ' . date('Y') . ' (Andika Prasatya - Portfolio)',
    'text_gitlab'   => 'https://gitlab.com/prasatya777'
];
